/**
 * @name mashukuk project
 *
 * @desc Proje kapsamı: Araç hacizlerini listelemek
 
 * Projenin işlem aşamları
 * 1.aşama(Web Scraping-Node.js)
    Kullanıcı girişi
    İstenilen tarih aralığında ve safahat türü ile safahat sorgulama
    Gelen verileri indirme
 * 2.aşama(Excel processing-python)
    4 avukattan gelen excel dosyalarının birleştirilmesi
    Mükerrer dosya numaralarının çıkarılması
 * 3.aşama((Web Scraping-Node.js)
    Excel dosyası verileri ile Dosya->Esas sorgulama
    Gelen sorgu verilerini görüntüleme
    Evrak sorgulama
    İstenilen tarih aralığında raporları pdf şeklinde indirme
 * 4.aşama(transfer pdf’s data to excel file)
    Pdflerdeki verileri istenilen şekilde verileri excele aktarma
 * Karşılaşılabilecek problemler
    VPN bağlantısında sürekli kopmalar meydana gelmesi
    MasHukuk Database sorgularının aşırı yavaş olması
    Sorgu sırasında sunucu bağlantılarının sürekli kesilmesi(sunucu içinde birden fazla istemci olmasından kaynaklanıyor.)
    2. aşamada birleştirilen excel verilerinin avukatlara özgü olması ve diğer avukatların bu verilerle kendi sistemlerinde Dosya->Esas verilerilerine erişememesi
 * Olası Çözümler
    Projenin python ve node.js olarak paralel yürütülmesi ve birden fazla kişinin aynı anda node.js ile ilgilenmemesi ya da node.js ile çalışacak kişilerin aynı sunucu üzerinden işlem yapmaması.
    Safahat dosyasından indirilen 4 excel dosyasının ayrı olarak işlenmesi gerekmektedir. İlk aşamada bu dosyaları birleştirmek hataya neden olacaktır.
   
 * pritunl https://client.pritunl.com/ + ssh key ile 4 avukatın hesabına girilebilir 
 * Listelenecek icra dosyalarının listesi, excel dosyasında MAS hukuk tarafından paylaşılacaktır. 
 * 4 avukatın kullanıcı sayfası üzerinden kullanıcı adı ve şifre ile giriş yapılacak
     http://10.66.0.141/Default.aspx
     https://10.66.0.142/Default.aspx
     https://10.66.0.143/Default.aspx
     https://10.66.0.144/Default.aspx
     Kullanıcı Adı	: mashukuk
     Parola 		    : 123456

 * Excel’deki icra dosya numaraları avukatların sayfalarındaki safahat sayfasında sorgulanacak.
 * SAFAHAT TÜRÜ alanına standart olarak “MAHRUMİYET BİLGİSİ EKLENDİ” sorgusu yazılacak
 * Listelenene veriler Excel dosyası olarak indirilecek. (Formatı örnektedir)
 * Bu örnek dosyadan toplamda, her kullanıcı için işlem yapılacağından, 4 adet olacak.
 * Mükerrer kayıtları çıkarılacak. Tüm exceller tek bir dosyada toplanacak ve o dosya da dosya numaraları mükerrer olanlar çıkartılacak.
 * Dosyaya girilecek.
 * Burada son 6 ay içinde eklenen tüm dosyalar indirilecek. Örnek dosyalar:
 * Maksimum 14 günlük tarih aralığını sorgulayabilirsiniz.
 * PDF te yer alan tüm veriler, (araç sahibi bilgileri, araç bilgileri, icra dosyası listeleri ) yan yana olacak şekilde EXCEL’ e aktarılacak
 * Sonuçlar mas hukuk ile paylaşılacak

 */
"use strict";

/***********
MODULES
************/
const puppeteer = require('puppeteer');
const moment = require('moment');
const XlsxPopulate = require('xlsx-populate');

/***********
DEFINITIONS
************/
var data_avukat = [];
var suanki_zaman;
var olddate, olddate_formatted;
var ikinci_yer_plus_ten_days, ikinci_yer_plus_ten_days_formatted;
var browser;
var error;
const MONTH_AMOUNT = 6;
const DIVIDER_AMOUNT = 14;
const DOWNLOAD_PATH = 'C:\\Users\\ertan\\Downloads\\puppeteer-veri-cekme-projesi'

/*********************************************************************
Consol log with time stamp
*********************************************************************/
async function log_timestamp(extra_log)
{
  let date_ob = new Date();
  let hours = date_ob.getHours();
  let minutes = date_ob.getMinutes();
  let seconds = date_ob.getSeconds();

  // prints time in HH:MM format
  console.log("[" + hours + ":" + minutes + ":" + seconds + "]: " + extra_log);
}

/*********************************************************************
Reset the date to the starting point
*********************************************************************/
async function date_reset_to_start()
{
  olddate = moment(suanki_zaman).subtract(MONTH_AMOUNT, 'months');
  olddate_formatted = moment(suanki_zaman).subtract(MONTH_AMOUNT, 'months').format("DD.MM.YYYY");
  ikinci_yer_plus_ten_days = moment(olddate).add(DIVIDER_AMOUNT, 'days');
  ikinci_yer_plus_ten_days_formatted = moment(olddate).add(DIVIDER_AMOUNT, 'days').format("DD.MM.YYYY");
}

/*********************************************************************
Add DIVIDER_AMOUNT days to the current date
*********************************************************************/
async function date_add_days()
{
  olddate_formatted = moment(olddate).add(DIVIDER_AMOUNT, 'days').format("DD.MM.YYYY")
  ikinci_yer_plus_ten_days_formatted = moment(ikinci_yer_plus_ten_days).add(DIVIDER_AMOUNT, 'days').format("DD.MM.YYYY");
  olddate = moment(olddate).add(DIVIDER_AMOUNT, 'days');
  ikinci_yer_plus_ten_days = moment(ikinci_yer_plus_ten_days).add(DIVIDER_AMOUNT, 'days');
}

/*********************************************************************
Merge downloaded xls files to one file
*********************************************************************/
async function merge_downloaded_excel(file_no)
{
  var fs = require('fs');
  const SAFAHAT_FILE = DOWNLOAD_PATH + '\\SAFAHAT.xlsx';
  const SAFAHAT_MERGED_FILE = DOWNLOAD_PATH + '\\SAFAHAT_' + '10.66.0.' + parseInt(file_no) + '.xlsx';

  if (fs.existsSync(SAFAHAT_MERGED_FILE)) {
    Promise.all([
      XlsxPopulate.fromFileAsync(SAFAHAT_FILE),
      XlsxPopulate.fromFileAsync(SAFAHAT_MERGED_FILE)
    ])
    .then(workbooks => {
      const workbook_downloaded = workbooks[0];
      const sheet_downloaded = workbook_downloaded.sheet(0);

      /* Delete first two rows and get the whole values from the current downloaded file */
      sheet_downloaded._rows.splice(1, 2);
      sheet_downloaded._rows.map((row, index) => {
        row._node.attributes.r = index;
      });

      /* Copy all values from the downloaded file to the merged file */
      try {
        const values_downloaded = sheet_downloaded.usedRange().value();
        const workbook_merged = workbooks[1];
        const sheet_merged = workbook_merged.sheet(0);
        const row_no_merged = sheet_merged.usedRange()._numRows;
        sheet_merged.cell("A" + parseInt(row_no_merged + 1)).value(values_downloaded);
        workbook_merged.toFileAsync(SAFAHAT_MERGED_FILE);
      }
      catch(error) {
        console.error('Empty excel file. Skip it');
      }
    });
  }
  else {
    console.log('Merged file doesn\'t exist. Copy the first downloaded file as the start point');
    var file = fs.createReadStream(SAFAHAT_FILE, { flags: 'r',  encoding: "binary",});
    var dest = fs.createWriteStream(SAFAHAT_MERGED_FILE, { flags: 'w',  encoding: "binary",});
    file.pipe(dest, { end: false });
  }
  /* Delete the downloaded file as it was merged */
  fs.unlinkSync(SAFAHAT_FILE);
}

/*********************************************************************
1. Open the web page and get the lawyer informations
*********************************************************************/
async function showAvukatBilgilerAll()
{
  try {
    /* Mark now as the starting point */
    suanki_zaman = moment();

    /* Remove the current downloaded folder */
    var fs = require('fs');
    fs.rmdirSync(DOWNLOAD_PATH, { recursive: true });

    /* Open a web browser */
    browser = await puppeteer.launch({ headless: false,defaultViewport: { 'width' :  1024, 'height' : 1000 }});
    const page = await browser.newPage();

    /* Login to the web site
        http://10.66.0.141/Default.aspx
        https://10.66.0.142/Default.aspx
        https://10.66.0.143/Default.aspx
        https://10.66.0.144/Default.aspx
        username	: mashukuk
        Password  : 123456

    */
    for(let i = 0; i < 4; i++) {
      /* Login to the current web site */
      var ip_website = 141 + i;
      data_avukat[i]='http://10.66.0.' + parseInt(ip_website) + '/LogIn.aspx';
      await page._client.send('Page.setDownloadBehavior',
                              {behavior: 'allow',
                              downloadPath: DOWNLOAD_PATH});
      await page.goto(data_avukat[i], { waitUntil: 'networkidle2' });
      await page.type('#username', 'mashukuk');
      await page.type('#password', '123456');
      await page.click('[name="logIn"]');
      await page.waitForNavigation();

      /* Search "MAHRUMİYET BİLGİSİ EKLENDİ" */
      await page.click('#h3Safahat');
      await page.type('#cbSafahatTurleri_I','MAHRUMİYET BİLGİSİ EKLENDİ.');
      await page.waitFor(1000);
      await page.keyboard.press('Enter');
      await page.waitFor(1000);

      /* Reset the date to the starting point */
      date_reset_to_start();
      var inputValue;
      /* Find the DIVIDER_AMOUNT days in the selected months */
      var days_pace_by_pace = parseInt(parseInt((suanki_zaman - moment(suanki_zaman).subtract(MONTH_AMOUNT, 'months')) / (1000 * 60 * 60 * 24)) / DIVIDER_AMOUNT);
      log_timestamp('----------------');
      log_timestamp('Lawyer website: ' + data_avukat[i]);
      log_timestamp('Days within selected months: ' + days_pace_by_pace);
      for (let j = 0; j < days_pace_by_pace + 1; j++) {
        log_timestamp('New start date: ' + olddate_formatted);
        log_timestamp('New finish date: ' + ikinci_yer_plus_ten_days_formatted);
      
        /* Cleanup date in safahatBaslangic and add the new date*/
        await page.click('#safahatBaslangic');
        inputValue = await page.$eval('#safahatBaslangic', el => el.value);
        log_timestamp('inputValue.length safahatBaslangic: ' + inputValue.length);
        for (let k = 0; k < inputValue.length; k++) {
          await page.keyboard.press("Backspace", {delay: 50});
        }
        await page.type('#safahatBaslangic', olddate_formatted);
        await page.keyboard.press('Enter');
        await page.waitFor(1000);

        /* Cleanup date in safahatBitis and add the new date*/
        await page.click('#safahatBitis');
        inputValue = await page.$eval('#safahatBitis', el => el.value);
        log_timestamp('inputValue.length safahatBitis: ' + inputValue.length)
        await page.type('#safahatBitis', 'selamlar');
        for (let k = 0; k < inputValue.length; k++) {
          await page.keyboard.press("Backspace", {delay: 50});
        }
        await page.type('#safahatBitis', ikinci_yer_plus_ten_days_formatted);
        await page.keyboard.press('Enter');
        await page.waitFor(1000);

        /* Push the search button */
        await page.click('#divSafahat > div > input');
      
        /* Wait till "İstek işleniyor lütfen bekleyiniz" from hidden->false to hidden->true */
        log_timestamp('wait till hidden true false')
        await page.waitForSelector('div.blockUI.blockMsg.blockPage', {hidden: false, timeout: 150000});
        log_timestamp('hidden: false')              
        await page.waitForSelector('div.blockUI.blockMsg.blockPage', {hidden: true, timeout: 150000});        
        log_timestamp('hidden: true')            
      
        /* Wait extra for the excel button */
        await page.waitForSelector('button.dt-button.buttons-excel.buttons-html5 > span', {timeout:1000});
      
        /* Push the excel button and download the new excel file */
        await page.click('button.dt-button.buttons-excel.buttons-html5 > span');
        await page.waitFor(1000);

        /* Merge downloaded excel to a file */
        await merge_downloaded_excel(ip_website);

        /* Add DIVIDER_AMOUNT days to the current time */
        await date_add_days();
      }
    }
  }
  catch (error) {
    console.log(error);
    browser.close();
  }
}

showAvukatBilgilerAll();